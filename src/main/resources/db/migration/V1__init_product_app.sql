create schema if not exists product;

create table product.product
(
    id          bigserial primary key,
    name        text  not null,
    price       float not null,
    description text
);

create table product.shopping_cart
(
    id            bigserial primary key,
    uuid          text      not null,
    date_created  timestamp not null default now(),
    date_modified timestamp not null default now()
);

create table product.shopping_cart_products
(
    id               bigserial primary key,
    shopping_cart_id bigint references product.shopping_cart (id) not null,
    product_id       bigint references product.product (id)       not null,
    quantity         bigint                                       not null check ( quantity>0 )
);

create index "index_shopping_cart_product"
    on product.shopping_cart_products using btree (shopping_cart_id);