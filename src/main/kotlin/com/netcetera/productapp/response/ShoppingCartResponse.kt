package com.netcetera.productapp.response

class ShoppingCartResponse (
    val shoppingCartUuid: String,
    val products: List<ProductResponse>,
    val totalPrice: Double
        )