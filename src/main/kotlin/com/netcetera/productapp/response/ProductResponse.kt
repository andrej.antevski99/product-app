package com.netcetera.productapp.response

class ProductResponse (
    val id: Long,
    val name: String,
    val price: Double,
    val description: String?
)