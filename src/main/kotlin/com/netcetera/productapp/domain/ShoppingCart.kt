package com.netcetera.productapp.domain

import java.time.ZonedDateTime
import javax.persistence.*

@Entity
@Table(name = "shopping_cart", schema = "product")
class ShoppingCart (

    @Column(name = "uuid")
    val uuid: String,

    @Column(name = "date_created")
    var dateCreated: ZonedDateTime = ZonedDateTime.now(),

    @Column(name = "date_modified")
    var dateLastModified: ZonedDateTime = ZonedDateTime.now(),

        ): BaseEntity()