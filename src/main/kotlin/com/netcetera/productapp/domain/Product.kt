package com.netcetera.productapp.domain

import javax.persistence.*

@Entity
@Table(name = "product", schema = "product")
class Product (
    @Column(name = "name")
    var name: String,

    @Column(name = "price")
    var price: Double,

    @Column(name = "description")
    var description: String?
        ): BaseEntity()