package com.netcetera.productapp.domain

import javax.persistence.*

@Entity
@Table(name = "shopping_cart_products", schema = "product")
class ShoppingCartProduct (

    @ManyToOne
    @JoinColumn(name = "product_id")
    val product: Product,

    @ManyToOne
    @JoinColumn(name = "shopping_cart_id")
    val shoppingCart: ShoppingCart,

    @Column(name = "quantity")
    var quantity: Long = 1L
    ): BaseEntity()