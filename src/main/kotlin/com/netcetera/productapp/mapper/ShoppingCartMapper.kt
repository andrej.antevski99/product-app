package com.netcetera.productapp.mapper

import com.netcetera.productapp.request.AddProductRequest
import com.netcetera.productapp.response.ShoppingCartResponse

interface ShoppingCartMapper {

    fun addProductToShoppingCart(addProductRequest: AddProductRequest): ShoppingCartResponse

    fun getPriceForShoppingCart(shoppingCartUuid: String): Double

    fun getProductsInShoppingCart(shoppingCartUuid: String): ShoppingCartResponse
}