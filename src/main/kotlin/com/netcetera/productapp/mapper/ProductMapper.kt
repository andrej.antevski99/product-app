package com.netcetera.productapp.mapper

import com.netcetera.productapp.request.ProductRequest
import com.netcetera.productapp.response.ProductResponse
import org.springframework.data.domain.Page

interface ProductMapper {

    fun saveProduct(productRequest: ProductRequest): ProductResponse

    fun getAllProductsPageable(pageNum: Int): Page<ProductResponse>
    fun getAllProducts(): List<ProductResponse>

}