package com.netcetera.productapp.mapper.impl

import com.netcetera.productapp.mapper.ProductMapper
import com.netcetera.productapp.request.ProductRequest
import com.netcetera.productapp.response.ProductResponse
import com.netcetera.productapp.service.ProductService
import com.netcetera.productapp.service.ProductService.Companion.mapProductToResponse
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class ProductMapperImpl(
    private val productService: ProductService
) : ProductMapper {

    override fun saveProduct(productRequest: ProductRequest): ProductResponse =
        with(productRequest) {
            mapProductToResponse(productService.createOrUpdate(id, name, price, description))
        }


    override fun getAllProductsPageable(pageNum: Int): Page<ProductResponse> =
        productService.findAllPageable(pageNum).map(::mapProductToResponse)

    override fun getAllProducts(): List<ProductResponse> =
        productService.findAll().map(::mapProductToResponse)

}