package com.netcetera.productapp.mapper.impl

import com.netcetera.productapp.domain.ShoppingCart
import com.netcetera.productapp.mapper.ShoppingCartMapper
import com.netcetera.productapp.request.AddProductRequest
import com.netcetera.productapp.response.ShoppingCartResponse
import com.netcetera.productapp.service.ProductService
import com.netcetera.productapp.service.ProductService.Companion.mapProductToResponse
import com.netcetera.productapp.service.ShoppingCartProductService
import com.netcetera.productapp.service.ShoppingCartService
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class ShoppingCartMapperImpl(
    private val shoppingCartService: ShoppingCartService,
    private val shoppingCartProductService: ShoppingCartProductService,
    private val productService: ProductService
) : ShoppingCartMapper {


    @Transactional
    override fun addProductToShoppingCart(addProductRequest: AddProductRequest): ShoppingCartResponse =
        with(addProductRequest) {
            val shoppingCart = if (shoppingCartUuid == null) {
                shoppingCartService.create()
            } else {
                shoppingCartService.findByUuid(shoppingCartUuid)
            }
            val product = productService.findById(productId)
            if (quantity >= 1) {
                shoppingCartProductService.add(shoppingCart, product, quantity)
            } else {
                shoppingCartProductService.remove(shoppingCart, product, -quantity)
            }

            getResponseForShoppingCart(shoppingCart)
        }


    override fun getPriceForShoppingCart(shoppingCartUuid: String): Double =
        getResponseForShoppingCart(shoppingCartService.findByUuid(shoppingCartUuid)).totalPrice

    override fun getProductsInShoppingCart(shoppingCartUuid: String): ShoppingCartResponse =
        getResponseForShoppingCart(shoppingCartService.findByUuid(shoppingCartUuid))

    private fun getResponseForShoppingCart(shoppingCart: ShoppingCart): ShoppingCartResponse {
        val productsInCart = shoppingCartProductService.findAllByShoppingCard(shoppingCart)

        val price = productsInCart.sumOf { it.quantity * it.product.price }

        return ShoppingCartResponse(shoppingCart.uuid, productsInCart.map { mapProductToResponse(it.product) }, price)
    }


}