package com.netcetera.productapp.repository

import com.netcetera.productapp.domain.Product
import com.netcetera.productapp.domain.ShoppingCart
import com.netcetera.productapp.domain.ShoppingCartProduct
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ShoppingCartProductRepository: JpaRepository<ShoppingCartProduct, Long> {

    fun findByShoppingCartAndProduct(shoppingCart: ShoppingCart, product: Product): ShoppingCartProduct?
    fun findAllByShoppingCart(shoppingCart: ShoppingCart): List<ShoppingCartProduct>
}