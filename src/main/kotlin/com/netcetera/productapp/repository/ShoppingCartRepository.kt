package com.netcetera.productapp.repository

import com.netcetera.productapp.domain.ShoppingCart
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.Optional

@Repository
interface ShoppingCartRepository: JpaRepository<ShoppingCart, Long> {

    fun findByUuid(uuid: String): ShoppingCart?

}