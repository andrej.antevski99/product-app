package com.netcetera.productapp.controller

import com.netcetera.productapp.mapper.ProductMapper
import com.netcetera.productapp.request.ProductRequest
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("api/products")
class ProductController (
    private val mapper: ProductMapper
        ) {

    @PostMapping
    fun saveProduct(@RequestBody request: ProductRequest) =
        mapper.saveProduct(request)

    @GetMapping("all/{pageNum}")
    fun getAllProducts(@PathVariable pageNum: Int = 0) =
        mapper.getAllProductsPageable(pageNum)

    @GetMapping("all")
    fun getAllProducts() =
        mapper.getAllProducts()

}