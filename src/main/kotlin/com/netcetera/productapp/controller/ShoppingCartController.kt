package com.netcetera.productapp.controller

import com.netcetera.productapp.mapper.ShoppingCartMapper
import com.netcetera.productapp.request.AddProductRequest
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/shopping-cart")
class ShoppingCartController(
    private val mapper: ShoppingCartMapper
) {

    @PostMapping
    fun addProductToShoppingCart(
        @RequestBody addProductRequest: AddProductRequest
    ) = mapper.addProductToShoppingCart(addProductRequest)

    @GetMapping("/{shoppingCartUuid}/price")
    fun getPriceForShoppingCart(
        @PathVariable shoppingCartUuid: String
    ) = mapper.getPriceForShoppingCart(shoppingCartUuid)

    @GetMapping("/{shoppingCartUuid}")
    fun getProductsInShoppingCart(
        @PathVariable shoppingCartUuid: String
    ) = mapper.getProductsInShoppingCart(shoppingCartUuid)
}
