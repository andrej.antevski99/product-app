package com.netcetera.productapp.request

class ProductRequest (
    val id: Long?,
    val name: String,
    val price: Double,
    val description: String?
)