package com.netcetera.productapp.request

class AddProductRequest (
    val shoppingCartUuid: String?,
    val productId: Long,
    val quantity: Long = 1
        )