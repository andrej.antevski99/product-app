package com.netcetera.productapp.service

import com.netcetera.productapp.domain.ShoppingCart
import com.netcetera.productapp.repository.ShoppingCartRepository
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.time.ZonedDateTime
import java.util.UUID

@Service
class ShoppingCartService (
    private val repository: ShoppingCartRepository
        ) {

    private val logger = LoggerFactory.getLogger(ShoppingCartService::class.java)

    fun create(): ShoppingCart {
        val uuid = UUID.randomUUID().toString()
        logger.info("Creating new shopping cart with uuid = $uuid")
        return repository.save(ShoppingCart(uuid))
    }

    fun update(id: Long): ShoppingCart {
        findById(id).let {
            it.dateLastModified = ZonedDateTime.now()
            return repository.save(it)
        }
    }

    fun findById(id: Long): ShoppingCart =
        repository.findById(id).orElseThrow { RuntimeException("Shopping with id = $id doesn't exist") }

    fun findByUuid(uuid: String): ShoppingCart =
        repository.findByUuid(uuid)
            ?: throw RuntimeException("Shopping with uuid = $uuid doesn't exist")


}