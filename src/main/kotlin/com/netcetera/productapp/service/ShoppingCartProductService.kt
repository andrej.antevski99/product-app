package com.netcetera.productapp.service

import com.netcetera.productapp.domain.Product
import com.netcetera.productapp.domain.ShoppingCart
import com.netcetera.productapp.domain.ShoppingCartProduct
import com.netcetera.productapp.repository.ShoppingCartProductRepository
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.lang.RuntimeException

@Service
class ShoppingCartProductService(
    private val repository: ShoppingCartProductRepository
) {

    private val logger = LoggerFactory.getLogger(ShoppingCartProductService::class.java)

    fun add(shoppingCart: ShoppingCart, product: Product, quantity: Long) {
        val existingItem = repository.findByShoppingCartAndProduct(shoppingCart, product)
        if (existingItem != null) {
            logger.info("Adding $quantity more products with id = ${product.id} to shopping cart with id = ${shoppingCart.id}")
            existingItem.quantity += quantity
            repository.save(existingItem)
        } else {
            logger.info("Adding new $quantity products with id = ${product.id} to shopping cart with id = ${shoppingCart.id}")
            repository.save(ShoppingCartProduct(product, shoppingCart, quantity))
        }
    }

    fun remove(shoppingCart: ShoppingCart, product: Product, quantity: Long) {
        val existingItem = repository.findByShoppingCartAndProduct(shoppingCart, product)
            ?: throw RuntimeException("The product doesn't exist in the shopping cart")
        if (existingItem.quantity <= quantity){
            repository.deleteById(existingItem.id)
        }else{
            existingItem.quantity -= quantity
            repository.save(existingItem)
        }
    }

    fun findAllByShoppingCard(shoppingCart: ShoppingCart): List<ShoppingCartProduct> =
        repository.findAllByShoppingCart(shoppingCart)


}