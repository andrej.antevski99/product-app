package com.netcetera.productapp.service

import com.netcetera.productapp.config.Constants
import com.netcetera.productapp.domain.Product
import com.netcetera.productapp.repository.ProductRepository
import com.netcetera.productapp.response.ProductResponse
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service

@Service
class ProductService (
    private val repository: ProductRepository
        ) {

    private val logger = LoggerFactory.getLogger(ProductService::class.java)


    fun createOrUpdate(id: Long?, name: String, price: Double, description: String?): Product =
        if (id != null) {
            update(id, name, price, description)
        } else {
            create(name, price, description)
        }

    private fun create(name: String, price: Double, description: String?): Product {
        logger.info("Creating product with name = $name ,price = $price, description = $description")
        return repository.save(Product(name, price, description))
    }

    private fun update(id: Long, name: String, price: Double, description: String?): Product {
        logger.info("Updating product with id=$id, setting name = $name ,price = $price, description = $description")
        findById(id).let {
            it.name = name
            it.price = price
            it.description = description
            return repository.save(it)
        }
    }

    fun findById(id: Long): Product =
        repository.findById(id).orElseThrow { RuntimeException("Product with id = $id doesn't exist") }


    fun findAllPageable(pageNum: Int): Page<Product> {
        val pageRequest = PageRequest.of(pageNum, Constants.DEFAULT_PAGE_SIZE)
        return repository.findAll(pageRequest)
    }
    fun findAll(): List<Product> {
        return repository.findAll()
    }

    companion object{
        fun mapProductToResponse(product: Product): ProductResponse =
            with(product) {
                ProductResponse(id, name, price, description)
            }
    }
}