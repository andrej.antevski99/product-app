package com.netcetera.productapp.config

class Constants {
    companion object {
        val DEFAULT_PAGE_SIZE = 10
    }
}